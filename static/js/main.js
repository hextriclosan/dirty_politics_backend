$.getJSON("/get_yulia_word_count",function(result){
        Highcharts.chart('container', {
            series: [{
                type: 'wordcloud',
                data: result,
                name: 'Frequency'
            }],
            title: {
                text: 'Wordcloud of Slugy Narodu'
            }
        });
});

