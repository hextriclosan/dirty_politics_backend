from pprint import pprint

from flask import render_template, Blueprint

from models.opinion import Opinion

web = Blueprint('web', __name__)


# a simple page that says hello
@web.route('/')
def hello():

    return render_template('landing.html', template_folder='templates')

@web.route('/authors')
def authors():
    opinions = Opinion.opinion_match(limit=50)
    pprint(opinions)
    return render_template('authors.html', template_folder='templates', opinions=[a['_source'] for a in opinions['hits']['hits']])
