from pprint import pprint

import requests


def get_yulia():
    r = requests.get('https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&explaintext&redirects=1&titles=Yulia_Tymoshenko')
    return r.json()


def tokenize(text):
    if text is not None:

        words = text.lower().split()
        return words
    else:
        return None


def map_text(tokens):
    hash_map = {}

    if tokens is not None:
        for element in tokens:
            # Remove Punctuation
            word = element.replace(",", "")
            word = word.replace(".", "")

            # Word Exist?
            if word in hash_map and word != "the":
                hash_map[word] = hash_map[word] + 1

            else:
                hash_map[word] = 1

        return hash_map
    else:
        return None


def count(text):
    # Tokenize the Book
    words = tokenize(text)

    # Create a Hash Map (Dictionary)
    map = map_text(words)

    return map

# wiki_page_json = get_yulia()
# text_lines = wiki_page_json['query']['pages']['1246843']['extract']
# pprint(count(text_lines))