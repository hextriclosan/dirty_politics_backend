# Required routes:
# - list candidates
# - Candidate add class + script
# - Candidate schema
# - keywords extraction and counting schema
import json
from datetime import datetime
from pprint import pprint

import flask
from dateutil.relativedelta import relativedelta

from flask import Blueprint, request

from blueprints.word_count import get_yulia, count
from models.opinion import Opinion, MAX_RESULTS
from scripts.print_contents import print_opinions

api = Blueprint('api', __name__)


@api.route('/post_opinion', methods=['POST'])
def post_opinion():
    op = Opinion(author=request.form.get('author'),
                 content=request.form.get('content'),
                 content_type=request.form.get('content_type'),
                 politic=request.form.get('politic')
                 )
    result = op.store()
    print(result)
    print("Received new opinion")
    pprint(op.to_dict())

    return flask.redirect("/authors", 302)


# Display candidate profile
@api.route('/opinions', methods=['GET'])
def get_profile_opinions():
    author = request.args.get('author')
    keywords = request.args.get('keywords')

    if keywords:
        keywords = keywords.split(',')

    politic = request.args.get('politic')
    since = request.args.get('since', datetime.now() + relativedelta(months=-1))
    until = request.args.get('until', datetime.now())

    text = request.args.get('text')
    limit = request.args.get('limit', MAX_RESULTS)
    page = request.args.get('page')

    # Get all relevant opinions for the past month
    opinions = Opinion.opinion_match(author=author, keywords=keywords,
                                     politic=politic,
                                     since=since,
                                     until=until,
                                     limit=limit, page=page)

    if not text:
        return json.dumps(opinions)
    else:
        return print_opinions(opinions)


@api.route('/get_yulia_word_count', methods=['GET'])
def get_yulia_word_count():
    wiki_page_json = get_yulia()
    text_lines = wiki_page_json['query']['pages']['1246843']['extract']

    weights = count(text_lines)

    from collections import defaultdict
    dct = defaultdict(list)

    for k, v in weights.items():
        dct[v].append(k)

    for k, v in dct.items():
        dct[k] = v[0]

    res = list()
    for each in sorted(dct.items()): #[-50:][::-1]
        res.append({'weight': each[0], 'name': each[1]})

    return json.dumps(res)# json.dumps()


