import os

from flask import Flask


def create_app():
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(SECRET_KEY='dev')

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from blueprints.api_routes import api
    from blueprints.web_routes import web

    app.register_blueprint(api)
    app.register_blueprint(web)

    return app


if __name__ == '__main__':

    app = create_app()
    app.config["CACHE_TYPE"] = "null"
    app.run(debug=True)


