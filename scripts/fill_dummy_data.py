import datetime
import time

from models.opinion import Opinion, OPINION_DOC
import random

if __name__ == "__main__":

    dummy_authors = ['vlad', 'zheka', 'ihor', 'kofe']
    dummy_politics = ['trump', 'pootin', 'ushenko', 'timoshenko', 'nakamoto', 'poroshenko']
    dummy_features = ['Stupid', 'Fucker', 'Bribed', 'Destructive', 'Inefficient', 'Forbidden', 'Go', 'Away', 'Ditch', 'Joke', 'Coke']

    n_opinions = 100
    max_features = 3

    for ID in range(1, 100000000):
        content = ' '.join([dummy_features[random.randint(0, len(dummy_features)-1)] for i in range(0, max_features)])

        a = Opinion(dummy_authors[random.randint(0, len(dummy_authors)-1)],
                    dummy_politics[random.randint(0, len(dummy_politics)-1)],
                    opinion_id=ID+time.time(), content=content, content_type=OPINION_DOC)
        a.store()
