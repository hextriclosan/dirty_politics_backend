from external.elastic_search import get_es
from models.opinion import OPINION_DOC, OPINIONS_INDEX


def print_opinions(response):
    str = ""
    for each in response['hits']['hits']:
        print(each)
        ts = each['_source']['timestamp'] or '...'
        content = each['_source']['content'] or '...'
        politic = each['_source']['politic'] or '...'
        str += ' '.join([ts, content, politic])
        str += '<br>'
    return str


if __name__ == "__main__":
    doc = {
        'size': 10000,
        'query': {
            'match_all': {}
        }
    }
    response = get_es().search(index=OPINIONS_INDEX, doc_type=OPINION_DOC, body=doc, scroll='1m')
    print_opinions(response)


