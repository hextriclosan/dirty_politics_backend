import main
from external.elastic_search import get_es
from models.opinion import OPINIONS_INDEX, OPINION_DOC

if __name__ == "__main__":

    get_es().indices.delete(index=OPINIONS_INDEX, ignore=[400, 404])
    get_es().indices.create(index=OPINIONS_INDEX, ignore=400)

    obj_mapping = {
        'properties': {
            'timestamp': {'type': 'date'},
        }
    }

    get_es().indices.put_mapping(OPINION_DOC, obj_mapping, [OPINIONS_INDEX])
