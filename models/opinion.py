import json
import time
from datetime import datetime
from pprint import pprint
from external.elastic_search import get_es

MAX_RESULTS = 1000

OPINIONS_INDEX = "opinions"
OPINION_DOC = 'opinion'
POLITIC = 'politic'
OPINION_ID = 'opinion_id'
CONTENT_TYPE = 'content_type'
TIMESTAMP = 'timestamp'
CONTENT = 'content'
AUTHOR = 'author'

OPINION_TYPE = {
    OPINION_DOC: "opinion",
    "like": "like"
}


class Opinion:
    def __init__(self, author, politic, content, content_type, opinion_id=time.time(), timestamp=datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")):
        self.author = author
        self.politic = politic
        self.opinion_id = opinion_id
        self.content = content
        self.content_type = OPINION_TYPE["opinion"]
        self.timestamp = timestamp

    def to_dict(self):
        return {
            AUTHOR: self.author,
            OPINION_ID: self.opinion_id,
            POLITIC: self.politic,
            CONTENT_TYPE: self.content_type,
            CONTENT: self.content,
            TIMESTAMP: datetime.now(),
        }

    def to_json(self):
        return json.dumps(self.to_dict())

    def store(self):
        res = get_es().index(index=OPINIONS_INDEX, doc_type=OPINION_DOC, body=self.to_dict())
        return res['result']

    # make sure to pass parameter "%Y-%m-%dT%H:%M:%S.%f"
    @staticmethod
    def opinion_match(keywords=None, politic=None, since=None,
                      until=datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f"),
                      author=None, page=1, limit=MAX_RESULTS):
        es = get_es()

        q = {
            'size': limit,
            "sort": [
                {"timestamp": {"order": "desc"}},
            ],
            "query": {}
        }

        params_amount = sum([1 if x is not None else 0 for x in [keywords, politic, author]])
        if params_amount > 1:
            q['query']['bool'] = {'must': []}
        elif params_amount == 1:
            q['query']['bool'] = {}
        elif params_amount == 0:
            #q['query'] = {'match_all': {}}
            pass

        if any([keywords, politic, author]):
            range_filter = True
        else:
            range_filter = False

        if any([since, until]):
            tq = {TIMESTAMP: {}}

            if since is not None:
                tq[TIMESTAMP]["gte"] = since

            if until is not None:
                tq[TIMESTAMP]["lte"] = until

            if range_filter:
                q['query']['bool']['filter'] = {"range": tq}
            else:
                q['query']["range"] = tq

        if politic is not None:
            if params_amount > 1:
                q['query']['bool']['must'].append({"match": {POLITIC: politic}})
            else:
                q['query'] = {"match": {POLITIC: politic}}

        if author is not None:
            if params_amount > 1:
                q['query']['bool']['must'].append({"match": {AUTHOR: author}})
            else:
                q['query'] = {"match": {AUTHOR: author}}

        if keywords is not None:
            if len(keywords) > 1:
                for key in keywords:
                    q['query']['bool']['must'].append({"match": {CONTENT: key}})
            else:
                if params_amount > 1:
                    for key in keywords:
                        q['query']['bool']['must'].append({"match": {CONTENT: key}})
                else:
                    for key in keywords:
                        q['query'] = {"match": {CONTENT: key}}

        pprint(q)
        try:
            res = es.search(index=OPINIONS_INDEX, body=q, scroll='1m')

            if page and int(page) > 1:
                for i in range(1, int(page)):
                    scroll = res['_scroll_id']
                    res = es.scroll(scroll_id=scroll, scroll='1m')

            return res
        except Exception as e:
            print(e)
            return 'Error'


if __name__ == "__main__":
    # print('-----------')
    # response = Opinion.opinion_match([], "ushenko", until=datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f"), author="zheka")
    # #pprint(response)
    pass


