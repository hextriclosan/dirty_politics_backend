from elasticsearch import Elasticsearch

ES_PORT = 9200
ES_HOST = '127.0.0.1'

es = None


def get_es():
    global es

    if not es:
        es = Elasticsearch(ES_HOST, port=ES_PORT)
    return es
